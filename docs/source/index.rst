.. dare-login documentation master file, created by
   sphinx-quickstart on Fri May 15 14:20:13 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to dare-login's documentation!
======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   app
   utils



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
