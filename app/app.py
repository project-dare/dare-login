import json
from logging.config import dictConfig
from os import environ

from flask import Flask, request, jsonify
from flask_swagger import swagger

import utils

dictConfig({
    'version': 1,
    'formatters': {'default': {
        'format': '[%(asctime)s] %(levelname)s in %(module)s: %(message)s',
    }},
    'handlers': {'wsgi': {
        'class': 'logging.StreamHandler',
        'stream': 'ext://flask.logging.wsgi_errors_stream',
        'formatter': 'default'
    }},
    'root': {
        'level': 'INFO',
        'handlers': ['wsgi']
    }
})
app = Flask(__name__)
logger = app.logger


@app.route('/auth', methods=['POST'])
def login():
    """
        Performs user authentication using Keycloak. The user needs to provide a username and a password in order
        to access the platform.

        Returns
            response: the response from keycloak
    """
    request_data = json.loads(request.data)
    requested_issuer = request_data.get("requested_issuer", "dare")
    token = request_data.get("access_token", None)
    if not token:
        if "username" not in request_data.keys() or "password" not in request_data.keys():
            msg = "No token or username/password were provided for auth!"
            return msg, 500
        auth_response = utils.authenticate_user(request_data["username"], request_data["password"], requested_issuer)
        if auth_response.status_code != 200:
            return auth_response.text, auth_response.status_code
        response = json.loads(auth_response.text)
        token = response["access_token"]
        result = utils.init_dare_services(token=token, username=request_data["username"],
                                          password=request_data["password"], logger=logger)
        logger.debug("Initialization result: {}".format(result))
        return response, auth_response.status_code
    else:
        username = request_data.get("username", None)
        password = request_data.get("password", None)
        if not username:
            user_data = utils.validate_token(token=token)
            username = user_data["username"]
        result = utils.init_dare_services(token=token, username=username, password=password, logger=logger)
        logger.debug("Initialization result: {}".format(result))
        return result


@app.route("/register", methods=["POST"])
def register():
    request_data = json.loads(request.data)
    requested_issuer = request_data["requested_issuer"] if "requested_issuer" in request_data.keys() else "dare"
    keycloak_config = utils.load_keycloak_config()
    url = keycloak_config["web"]["users_uri"]
    admin_credentials = keycloak_config["admin_credentials"]
    admin_username = environ["ADMIN_USERNAME"] if "ADMIN_USERNAME" in environ.keys() else admin_credentials["username"]
    admin_password = environ["ADMIN_PASSWORD"] if "ADMIN_PASSWORD" in environ.keys() else admin_credentials["password"]
    auth_response = utils.authenticate_user(admin_username, admin_password, requested_issuer)
    if auth_response.status_code == 200:
        access_token = json.loads(auth_response.text)["access_token"]
        response = utils.register_user(url=url, access_token=access_token, username=request_data["username"],
                                       password=request_data["password"], first_name=request_data["first_name"],
                                       last_name=request_data["last_name"], email=request_data["email"])
        return response.text, response.status_code
    else:
        return "Could not authenticate admin user", 500


@app.route('/validate-token', methods=['POST'])
def validate_token():
    """
    API endpoint to validate a given token
    """
    data = json.loads(request.data)
    full_resp = data["full_resp"] if "full_resp" in data.keys() else None
    user_data = utils.validate_token(token=data["access_token"], full_resp=full_resp)
    if not user_data:
        return "User is not authenticated", 500
    else:
        return json.dumps(user_data), 200


@app.route('/delegation-token', methods=['POST'])
def issue_delegation_token():
    """
    API endpoint to issue a delegation token
    """
    data = json.loads(request.data)
    full_resp = data["full_resp"] if "full_resp" in data.keys() else None
    response = utils.validate_token(token=data["access_token"], full_resp=full_resp)
    if response:
        resp = utils.issue_delegation_token(token=data["access_token"], issuer=response["issuer"])
        if resp.status_code == 200:
            access_token = json.loads(resp.text)["access_token"]
            return json.dumps({"access_token": access_token}), 200
        else:
            error_description = json.loads(resp.text)
            return "Error! Could not issue a delegation token: {}".format(error_description), 500
    else:
        return "Invalid token!", 500


@app.route('/refresh-token', methods=["POST"])
def refresh_token():
    """
    API endpoint to refresh a token
    """
    data = json.loads(request.data)
    response = utils.refresh_token(data["refresh_token"])
    return response.text, response.status_code


@app.route("/docs")
def spec():
    return jsonify(swagger(app))
