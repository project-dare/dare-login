import json
import os
import sys

import requests


def login(hostname, username, password, requested_issuer):
    data = {
        "username": username,
        "password": password,
        "requested_issuer": requested_issuer
    }
    headers = {"Content-Type": "application/json"}
    return requests.post(hostname + '/auth', data=json.dumps(data), headers=headers)


dare_login_service_host = os.environ["DARE_LOGIN_PUBLIC_SERVICE_HOST"]
dare_login_service_port = os.environ["DARE_LOGIN_PUBLIC_SERVICE_PORT"]
admin_username = sys.argv[1]
admin_password = sys.argv[2]
response = login(hostname="http://{}:{}".format(dare_login_service_host, dare_login_service_port),
                 username=admin_username, password=admin_password, requested_issuer="dare")
